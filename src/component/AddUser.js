// import React, {useState} from "react";
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

class AddUser extends Component{

    onChange = (e) => {
        const state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    onSubmit = (e) => {
        e.preventDefault();

        const {studentIndex, studentName, studentAddress, studentTelephone} = this.state;

        axios.post('http://localhost:8080/student/save', {studentIndex, studentName, studentAddress, studentTelephone})
            .then((result) => {
                // console.log(result.data);
                this.props.history.push("/view")
            });
    };

    constructor() {
        super();
        this.state = {
            studentIndex: '',
            studentName: '',
            studentAddress: '',
            studentTelephone: ''
        };
    }


    render() {
        const {studentIndex, studentName, studentAddress, studentTelephone} = this.state;

        return (
            <div className="row justify-content-center">
                <div className="card w-25 m-5">
                    <div className="card-body">
                        <h4>User Registration Form</h4>
    
                        <form>
    
                            <div className="form-group mt-3 ">
                                <label htmlFor="userName" className="float-left">Input Index : </label>
                                <input type="number" className="form-control" name="studentIndex" value={studentIndex}
                                       onChange={this.onChange}/>
                            </div>
    
                            <div className="form-group">
                                <label htmlFor="userName" className="float-left">Input Name : </label>
                                <input type="text" className="form-control" name="studentName" value={studentName}
                                       onChange={this.onChange}/>
                            </div>
    
                            <div className="form-group mt-3 ">
                                <label htmlFor="userName" className="float-left">Input Address : </label>
                                <textarea className="form-control" name="studentAddress" value={studentAddress}
                                          onChange={this.onChange}/>
                            </div>
    
                            <div className="form-group">
                                <label htmlFor="userName" className="float-left">Input Telephone : </label>
                                <input type="tel" className="form-control" name="studentTelephone" value={studentTelephone}
                                       onChange={this.onChange}/>
                            </div>
    
                            <button className="btn btn-primary float-left" type="button" onClick={this.onSubmit}>Save</button>
    
                        </form>

                    </div>
                </div>
    
            </div>
        );
    }

}

export default AddUser;
