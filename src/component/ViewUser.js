import React, {Component} from 'react';

import axios from 'axios';
import AddUser from './AddUser';

class ViewUser extends Component{

    onChange = (e) => {
        const state = this.state.child;
        state[e.target.name] = e.target.value;
        this.setState({child : state});
    };

    constructor(props) {
        super(props);
        this.state = {
            studentId : 0,
            studentIndex: '',
            studentName: '',
            studentAddress: '',
            studentTelephone: '',
            child : {studentIndex : "",studentName:"",studentAddress:"",studentTelephone:""},
            students : []
        };
      }

    componentDidMount() {
        axios.get('http://localhost:8080/student/all')
            .then(res => {
                this.setState({ students : res.data });
            });
    }

    onDelete(id){

        axios.delete('http://localhost:8080/student/delete/'+id)
            .then(res => {
                console.log(res.data);
                this.props.history.push("/");
            });
    }

    onSearch(id) {

        this.studentId = id;

        axios.get('http://localhost:8080/student/search/'+id)
            .then(res => {
                this.setState({ child : res.data });
            });
    }

    onUpdate = (e) => {

        e.preventDefault();

        const {studentIndex, studentName, studentAddress, studentTelephone} = this.state.child;

        axios.put('http://localhost:8080/student/update/'+this.studentId,{studentIndex, studentName, studentAddress, studentTelephone})
            .then(res => {
                console.log(res.data);
            });
    }


    render(){

        return(
            <div className="row justify-content-center">
                <div className="card">
                    <div className="card-body">
                        <h4>view User</h4>

                        <form>
    
                            <div className="form-group mt-3 ">
                                <label htmlFor="userName" className="float-left">Input Index : </label>
                                <input type="number" className="form-control" name="studentIndex" value={this.state.child.studentIndex}
                                       onChange={this.onChange}/>
                            </div>
    
                            <div className="form-group">
                                <label htmlFor="userName" className="float-left">Input Name : </label>
                                <input type="text" className="form-control" name="studentName" value={this.state.child.studentName}
                                       onChange={this.onChange}/>
                            </div>
    
                            <div className="form-group mt-3 ">
                                <label htmlFor="userName" className="float-left">Input Address : </label>
                                <textarea className="form-control" name="studentAddress" value={this.state.child.studentAddress}
                                          onChange={this.onChange}/>
                            </div>
    
                            <div className="form-group">
                                <label htmlFor="userName" className="float-left">Input Telephone : </label>
                                <input type="tel" className="form-control" name="studentTelephone" value={this.state.child.studentTelephone}
                                       onChange={this.onChange}/>
                            </div>
    
                            <button className="btn btn-primary float-left" type="button" onClick={this.onUpdate}>Update</button>
    
                        </form>

                        <table className="table table-striped">
                            <thead>
                                <tr>
                                <th>Student Index</th>
                                <th>Student Name</th>
                                <th>Student Address</th>
                                <th>Student Telephone</th>
                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.students.map(
                                        student =>
                                        <tr key={student.id}>
                                            <td>{student.studentIndex}</td>
                                            <td>{student.studentName}</td>
                                            <td>{student.studentAddress}</td>
                                            <td>{student.studentTelephone}</td>
                                            <td>
                                                <button className="btn btn-danger" onClick={() => this.onDelete(student.id)}>Delete</button>
                                                <button className="btn btn-success" onClick={() => this.onSearch(student.id)}>Edit</button>
                                            </td>
                                       </tr>
                                    )
                                }
                            </tbody>
                        </table>
                        

                    </div>
                </div>
    
            </div>
        );
    }

}

export default ViewUser;