import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

class SignUser extends Component{

    constructor() {
        super();
        this.state = {
            userName: '',
            userType: '',
            userPassword: ''
        };
    }

    onChange = (e) => {
        const state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    onSubmit = (e) => {
        e.preventDefault();

        const {userName,userType,userPassword} = this.state;

        axios.post('http://localhost:8080/users/add', {userName,userType,userPassword})
            .then((result) => {
                console.log(result.data);
                // this.props.history.push("/")
            });
    };


    render(){

        const {userName,userType,userPassword} = this.state;

        return(
            <div className="row justify-content-center">
                <div className="card w-25 m-5">
                    <div className="card-body">
                        <h4>User Registration Form</h4>
    
                        <form>
    
                            <div className="form-group mt-3 ">
                                <label htmlFor="userName" className="float-left">UserName : </label>
                                <input type="text" className="form-control" name="userName" value={userName}
                                       onChange={this.onChange}/>
                            </div>
    
                            <div className="form-group">
                                <label htmlFor="userType" className="float-left">User Type : </label>
                                <input type="text" className="form-control" name="userType" value={userType}
                                       onChange={this.onChange}/>
                            </div>
    
                            <div className="form-group">
                                <label htmlFor="userPassword" className="float-left">User Password : </label>
                                <input type="tel" className="form-control" name="userPassword" value={userPassword}
                                       onChange={this.onChange}/>
                            </div>
    
                            <button className="btn btn-primary float-left" type="button" onClick={this.onSubmit}>Sign Up</button>
    
                        </form>

                    </div>
                </div>
    
            </div>
        );
    }

}

export default SignUser;