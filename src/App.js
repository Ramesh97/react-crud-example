import React from 'react';
import './App.css';
import AddUser from "./component/AddUser";
import ViewUser from './component/ViewUser';
import SignUser from './component/SignUser';

function App() {
  return (
    <div className="App">
      <AddUser/>
      {/* <ViewUser /> */}
    </div>
  );
}

export default App;
